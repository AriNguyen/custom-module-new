from odoo import models, fields, api


class student_contact(models.Model):
    _description = 'student_contact.student_contact'
    _inherit = 'res.partner'

    name = fields.Char(string = "Student Name")
    DOB = fields.Date(string="Student DOB")

    Class  = fields.Char(string="Student Class")
