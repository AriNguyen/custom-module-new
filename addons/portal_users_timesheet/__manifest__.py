# -*- coding: utf-8 -*-
# Part of Odoo. See COPYRIGHT & LICENSE files for full copyright and licensing details.

{
    'name': 'Portal TimeSheet',
    'version': '1.0.2',
    'summary': 'Portal employee can manage time sheet.',
    'sequence': 20,
    'description': """
        Portal users can manage their time sheets by website portal without any interaction with backend side.

        Portal
        Time sheet
        Project
        User
        Task
        HR
        Employee
        Portal timesheet
        timesheet
    """,
    'category': 'Project',
    'author': 'Synconics Technologies Pvt. Ltd.',
    'website': 'http://www.synconics.com',
    'depends': ['portal', 'website', 'hr_timesheet'],
    'data': [
        'security/ir.model.access.csv',
        'security/hr_timesheet_security.xml',
        'views/hr_timesheet_portal_templates.xml',
    ],
    'demo': [],
    'images': ['static/description/main_screen.jpg'],
    'license': 'OPL-1',
    'price': 199.0,
    'currency': 'EUR',
    'installable': True,
    'application': True,
    'auto_install': False,
}
