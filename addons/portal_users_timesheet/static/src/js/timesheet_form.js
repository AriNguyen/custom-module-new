odoo.define("website.timesheet_form", function (require) {
    "use strict";

    require('web.dom_ready');
    var ajax = require('web.ajax');
    var rpc = require('web.rpc');

    $('#project_id').on('change', function () {
        var selecttasks = $("select[name='task_id']");
        selecttasks.html('');
        if (!$("#project_id").val()) {
            return;
        } else{
            ajax.jsonRpc("/timesheet/project_infos/", 'call', {'project_id': $("#project_id").val()}).then(function (data) {
                if (selecttasks.data('init')===0 || selecttasks.find('option').length===0) {
                    if (data.length) {
                        _.each(data, function (x) {
                            var opt = $('<option>').text(x.name)
                                .attr('value', x.id)
                                .attr('data-code', x.name);
                            selecttasks.append(opt);
                        });
                        selecttasks.parent('div').show();
                    } else {
                        var opt = $('<option>').text('')
                            .attr('value', '')
                            .attr('data-code', '');
                        selecttasks.append(opt);
                        selecttasks.parent('div').show();
                    }
                    selecttasks.data('init', 0);
                } else {
                    selecttasks.data('init', 0);
                }
            });
        }
    });
});
