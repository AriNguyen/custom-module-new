# -*- coding: utf-8 -*-
# Part of Odoo. See COPYRIGHT & LICENSE files for full copyright and licensing details.

from collections import OrderedDict
from operator import itemgetter
from datetime import datetime

from odoo import fields, http, _
from odoo.http import request
from odoo.tools import date_utils, groupby as groupbyelem
from odoo.osv.expression import AND, OR
from ast import literal_eval
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.exceptions import UserError, ValidationError
from odoo.addons.portal.controllers.portal import pager as portal_pager
from odoo.addons.hr_timesheet.controllers.portal import TimesheetCustomerPortal


class TimesheetCustomerPortal(TimesheetCustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(TimesheetCustomerPortal, self)._prepare_portal_layout_values()
        domain = request.env['account.analytic.line']._timesheet_get_portal_domain()
        employee_domain = []
        # Portal User Wise Filtered
        if request.env.user.has_group('base.group_portal'):
            domain = AND([domain, [('user_id', '=', request.env.user.id)]])
            employee_domain = [('user_id', '=', request.env.user.id)]

        employees = request.env['hr.employee'].sudo().search(employee_domain)

        values['timesheet_count'] = request.env['account.analytic.line'].sudo().search_count(domain)
        values['employees'] = employees
        return values

    @http.route(['/timesheet/project_infos/'], type='json', auth="public", methods=['POST'], website=True)
    def project_infos(self, project_id, **kw):
        tasks = request.env['project.task'].search_read([('project_id', '=', int(project_id))], ['id', 'name'])
        return tasks

    def _timesheet_get_page_view_values(self, timesheet, access_token, **kwargs):
        values = {
            'page_name': 'timesheet',
            'timesheet': timesheet,
        }
        return self._get_page_view_values(timesheet, access_token, values, 'my_timesheets_history', False, **kwargs)

    @http.route(['/my/timesheets', '/my/timesheets/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_timesheets(self, page=1, sortby=None, filterby=None, search=None, search_in='all', groupby='project', **kw):
        response = super(TimesheetCustomerPortal, self).portal_my_timesheets(page=page, sortby=sortby, filterby=filterby, search=search, search_in=search_in, groupby=groupby, **kw)
        Timesheet_sudo = request.env['account.analytic.line'].sudo()
        domain = request.env['account.analytic.line']._timesheet_get_portal_domain()
        searchbar_filters = response.qcontext.get('searchbar_filters')
        user_id = request.env.user

        # Portal User Wise Filtered
        if user_id.has_group('base.group_portal'):
            domain = AND([domain, [('user_id', '=', user_id.id)]])

        response.qcontext['searchbar_inputs'].update({
            'date': {'input': 'date', 'label': _('Search in Date')},
            'employee': {'input': 'employee', 'label': _('Search in Employee')},
            'task': {'input': 'task', 'label': _('Search in Task')},
            'project': {'input': 'project', 'label': _('Search in Project')},
        })

        response.qcontext['searchbar_groupby'].update({
            'task': {'input': 'task', 'label': _('Task')},
            'employee': {'input': 'employee', 'label': _('Employee')},
        })

        # search
        if search and search_in:
            search_domain = []
            if search_in in ('employee', 'all'):
                search_domain = OR([search_domain, ['|', ('name', 'ilike', search),('employee_id', 'ilike', search)]])
            if search_in in ('task', 'all'):
                search_domain = OR([search_domain, [('task_id', 'ilike', search)]])
            if search_in in ('project', 'all'):
                search_domain = OR([search_domain, [('project_id', 'ilike', search)]])
            if search_in in ('date', 'all'):
                search_domain = OR([search_domain, [('date', 'ilike', search)]])
            domain += search_domain

        # default filter by value
        if not filterby:
            filterby = 'all'
        domain = AND([domain, searchbar_filters[filterby]['domain']])

        # default sort by value
        if not sortby:
            sortby = 'date'
        order = response.qcontext['searchbar_sortings'][sortby]['order']
        timesheet_count = Timesheet_sudo.search_count(domain)
        # pager
        pager = portal_pager(
            url="/my/timesheets",
            url_args={'sortby': sortby, 'search_in': search_in, 'search': search, 'filterby': filterby},
            total=timesheet_count,
            page=page,
            step=self._items_per_page
        )

        if groupby == 'task':
            order = "task_id, %s" % order
        if groupby == 'employee':
            order = "employee_id, %s" % order

        timesheets = Timesheet_sudo.search(domain, order=order, limit=self._items_per_page, offset=pager['offset'])
        if groupby == 'project':
            grouped_timesheets = [Timesheet_sudo.concat(*g) for k, g in groupbyelem(timesheets, itemgetter('project_id'))]
        elif groupby == 'task':
            grouped_timesheets = [Timesheet_sudo.concat(*g) for k, g in groupbyelem(timesheets, itemgetter('task_id'))]
        elif groupby == 'employee':
            grouped_timesheets = [Timesheet_sudo.concat(*g) for k, g in groupbyelem(timesheets, itemgetter('employee_id'))]
        else:
            grouped_timesheets = [timesheets]

        domain_project = [('allow_timesheets', '=', True)]
        domain_task = []
        employee_domain = []
        # Portal User Wise Filtered
        if user_id.has_group('base.group_portal'):
            domain_project = [('allow_timesheets', '=', True), ('privacy_visibility', '=', 'portal'), ('message_partner_ids', 'child_of', [user_id.partner_id.commercial_partner_id.id])]
            domain_task = [('project_id.privacy_visibility', '=', 'portal'), ('message_partner_ids', 'child_of', [user_id.partner_id.commercial_partner_id.id])]
            employee_domain = [('user_id', '=', user_id.id)]
        elif user_id.has_group('base.group_user') and user_id.has_group('project.group_project_user'):
            #Internal User Wise Filtered
            domain_project = [('allow_timesheets', '=', True), '|', ('privacy_visibility', '!=', 'followers'), ('message_partner_ids', 'child_of', [user_id.partner_id.commercial_partner_id.id])]
            domain_task = ['|', ('project_id.privacy_visibility', '!=', 'followers'), ('message_partner_ids', 'child_of', [user_id.partner_id.commercial_partner_id.id])]

        # content according to pager and archive selected
        timesheets = Timesheet_sudo.search(domain, order=order, limit=self._items_per_page, offset=response.qcontext['pager']['offset'])
        request.session['my_timesheets_history'] = timesheets.ids[:100]

        projects = request.env['project.project'].sudo().search(domain_project)
        tasks = request.env['project.task'].sudo().search(domain_task)
        employees = request.env['hr.employee'].sudo().search(employee_domain)
        if not employees:
            raise request.not_found()

        response.qcontext.update({
            'projects': projects,
            'employees': employees,
            'tasks': tasks,
            'timesheets': timesheets,
            'grouped_timesheets': grouped_timesheets,
            'search_in': search_in,
            'sortby': sortby,
            'groupby': groupby,
            'pager': pager,
            'searchbar_filters': OrderedDict(sorted(searchbar_filters.items())),
            'filterby': filterby,
            })
        return request.render("portal_users_timesheet.portal_my_timesheets", response.qcontext)

    @http.route(['/create/timesheet'], type='http', auth="public", website=True, csrf=False)
    def create_timesheet(self, **post):
        """
            Create method for timesheet.
        """
        redirect = ("/my/timesheets")
        try:
            # Unit Amount Cal
            hours = post.get('unit_amount_hours') or 0
            minutes = post.get('unit_amount_minutes') or 0
            unit_amount = self.convert_float(hours, minutes)

            if not post.get('task_id'):
                raise ValidationError("There is no any task is associated with this project.")
            if post.get('date'):
                GivenDate = datetime.strptime(post.get('date'), DF).date()
                if GivenDate and (GivenDate > fields.Date.today()):
                    raise UserError("Date format must be less than or equal to today's date.")
            employee_id = False
            if post.get('employee_id'):
                employee_id = literal_eval(post.get('employee_id'))

            vals = {
                'name': post.get('name'),
                'task_id': literal_eval(post.get('task_id')) or False,
                'employee_id': employee_id,
                'date': post.get('date'),
                'project_id': literal_eval(post.get('project_id')) or False,
                'unit_amount': unit_amount,
                'user_id': request.env.user.id,
            }
            try:
                request.env['account.analytic.line'].sudo().create(vals)
                return request.redirect(redirect)
            except Exception as e:
                return request.redirect("%s?error_msg=%s" % (redirect, (e.name or e.value)))

        except Exception as e:
            return request.redirect("%s?error_msg=%s" % (redirect, (e.name or e.value)))

    @http.route(['/my/timesheet/<int:timesheet_id>'], type='http', auth="user", website=True, csrf=False)
    def portal_my_timesheet(self, timesheet_id=None, access_token=None, **kw):
        redirect = ("/my/timesheets")
        try:
            # Timesheet_sudo = request.env['account.analytic.line'].sudo()
            timesheet_sudo = self._document_check_access('account.analytic.line', timesheet_id, access_token)
            domain_project = [('allow_timesheets', '=', True)]
            domain_task = []
            employee_domain = []
            # Portal User Wise Filtered
            if request.env.user.has_group('base.group_portal'):
                domain_project = [('allow_timesheets', '=', True), ('privacy_visibility', '=', 'portal'),
                                  ('message_partner_ids', 'child_of', [request.env.user.partner_id.commercial_partner_id.id])]
                domain_task = [('project_id.privacy_visibility', '=', 'portal'),
                               ('message_partner_ids', 'child_of', [request.env.user.partner_id.commercial_partner_id.id])]
                employee_domain = [('user_id', '=', request.env.user.id)]
            projects = request.env['project.project'].sudo().search(domain_project)
            tasks = request.env['project.task'].sudo().search(domain_task)
            employees = request.env['hr.employee'].sudo().search(employee_domain)

            values = self._timesheet_get_page_view_values(timesheet_sudo, access_token, **kw)
            values.update({
                'projects': projects,
                'employees': employees,
                'tasks': tasks
            })
            return request.render("portal_users_timesheet.portal_my_timesheet", values)
        except Exception as e:
            return request.redirect("%s?error_msg=%s" % (redirect, (e.name or e.value)))

    @http.route(['/my/timesheet/delete/<int:timesheet_id>'], type='http', auth="user", website=True, csrf=False)
    def portal_unlink_timesheet(self, timesheet_id=None, access_token=None, **kw):
        redirect = '/my/timesheets'
        try:
            timesheet_sudo = self._document_check_access('account.analytic.line', timesheet_id, access_token)
            if timesheet_sudo:
                timesheet_sudo.unlink()
            return request.redirect(redirect)
        except Exception as e:
            return request.redirect("%s?error_msg=%s" % (redirect, (e.name or e.value)))

    @http.route(['/timesheet/update/<model("account.analytic.line"):timesheet_id>'], type='http', auth="user", methods=['POST'], website=True, csrf=False)
    def update_timesheet(self, timesheet_id, **post):
        """
            Update method for timesheet.
        """
        redirect = ("/my/timesheet/%s" % timesheet_id.id)
        try:
            # Unit Amount Cal
            hours = post.get('unit_amount_hours') or 0
            minutes = post.get('unit_amount_minutes') or 0
            unit_amount = self.convert_float(hours, minutes)

            if not post.get('task_id'):
                raise ValidationError("There is no any task is associated with this project.")

            if post.get('date'):
                GivenDate = datetime.strptime(post.get('date'), DF).date()
                if GivenDate and (GivenDate > fields.Date.today()):
                    raise UserError("Date format must be less than or equal to today's date.")

            employee_id = False
            if post.get('employee_id'):
                employee_id = literal_eval(post.get('employee_id'))

            if timesheet_id:
                timesheet_id.check_access_rights('write')
                timesheet_id.sudo().update({
                    'name': post.get('name'),
                    'task_id': literal_eval(post.get('task_id')),
                    'employee_id': employee_id,
                    'date': post.get('date'),
                    'project_id': literal_eval(post.get('project_id')),
                    'unit_amount': unit_amount,
                })
            return request.redirect('/my/timesheets')
        except Exception as e:
            return request.redirect("%s?error_msg=%s" % (redirect, (e.name or e.value)))

    def convert_float(self, hours, minutes):
        factor = 1
        hours = int(hours)
        minutes = int(minutes)
        return factor * (hours + (minutes / 60))
